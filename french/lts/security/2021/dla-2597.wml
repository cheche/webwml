#use wml::debian::translation-check translation="66af9e8afbf4d4c7f978a1130674ac0eec6e1425" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité de script intersite (XSS)
dans <tt>velocity-tools</tt>, une collection d’outils utiles pour le moteur de
patron <q>Velocity</q>.</p>

<p>La page d’erreur par défaut pourrait être exploitée pour dérober les cookies
de session, pour réaliser des requêtes au nom de la victime ou pour des
attaques par hameçonnage ou d’autres attaques similaires.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13959">CVE-2020-13959</a>

<p>La page d’erreur par défaut pour VelocityView dans les outils d’Apache
Velocity avant la version 3.1 renvoie à un fichier vm qui était entré comme
partie de l’URL. Un attaquant peut régler un fichier de charge XSS comme fichier
vm dans cette URL ce qui aboutit à ce que cette charge soit exécutée. Les
vulnérabilités XSS permettent à des attaquants d’exécuter du JavaScript
arbitraire dans le contexte du site web attaqué et de l’utilisateur attaqué.
Cela peut être utilisé pour dérober des cookies de session, pour réaliser des
requêtes au nom de la victime ou pour des attaquer par hameçonnage.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2.0-6+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets velocity-tools.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2597.data"
# $Id: $
