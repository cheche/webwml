#use wml::debian::translation-check translation="75b2fef34f659dd0d781b4552589f90b9734ded9" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La mise à jour de apache2 publiée sous DLA-1900-1 fournissait un correctif
incomplet pour
<a href="https://security-tracker.debian.org/tracker/CVE-2019-10092">CVE-2019-10092</a>,
un problème limité de script intersite affectant la page d’erreur mod_proxy.
L’ancien correctif introduisait plutôt une nouvelle protection CSRF qui causait
une régression, une incapacité à modifier dynamiquement l’état des membres dans
l’équilibreur de charge à l’aide de son gestionnaire. Cette mise à jour revient
sur cette modification et fournit le correctif correct de l’amont pour corriger
<a href="https://security-tracker.debian.org/tracker/CVE-2019-10092">CVE-2019-10092</a>.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 2.4.10-10+deb8u16.</p>
<p>Nous vous recommandons de mettre à jour vos paquets apache2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1900-2.data"
# $Id: $
