#use wml::debian::translation-check translation="adc5cbd36ecf754028e80bbdee567a58bca03b81" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9591">CVE-2016-9591</a>

<p>Utilisation de mémoire après libération de tas dans jas_matrix_destroy. Cette
vulnérabilité existe dans le code responsable du réencodage d’un fichier d’entrée
d’image décodée en une image JP2. La vulnérabilité est causée en ne réglant pas
les pointeurs relatifs à null après la libération des pointeurs (c'est-à-dire,
des opérations Setting-Pointer-Null manquantes après libération). La
vulnérabilité peut après coup provoquer une double libération de zone de
mémoire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10251">CVE-2016-10251</a>

<p>Un dépassement d'entier dans la fonction jpc_pi_nextcprl dans jpc_t2cod.c dans
JasPer avant 1.900.20 permet à des attaquants distants d’avoir un impact non
précisé à l'aide d'un fichier contrefait, qui déclenche l’utilisation d’une
valeur non initialisée.</p>

<li>Correctifs supplémentaires pour TEMP-CVE de la dernière publication pour
éviter des ennuis avec SIZE_MAX.</li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.900.1-13+deb7u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets jasper.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-920.data"
# $Id: $
