#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Maor Shwartz, Jeremy Heng et Terry Chia ont découvert deux vulnérabilités de
sécurité dans Graphicsmagick, une collection d’outils de traitement d'image.
</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16352">CVE-2017-16352</a>

<p>Graphicsmagick était vulnérable à un dépassement de tampon basé sur le tas
trouvé dans la caractéristique <q>Display visual image directory</q> de la
fonction DescribeImage() du fichier magick/describe.c. Une façon de faire
apparaitre la vulnérabilité est d’exécuter la commande identify sur un fichier
contrefait pour l'occasion au format MIFF avec l’option verbose.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16353">CVE-2017-16353</a>

<p>Graphicsmagick était vulnérable à une divulgation d'informations de mémoire
trouvée dans la fonction DescribeImage du fichier magick/describe.c, dû à une
lecture hors limites de tampon basé sur le tas. La portion de code contenant la
vulnérabilité est chargée d’afficher l’information de profil IPTC contenue dans
l’image. Cette vulnérabilité peut être provoquée à l’aide d’un fichier MIFF
spécialement contrefait. Un déréférencement de tampon hors limites survient dû
à ce que certains incréments ne sont pas vérifiés.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.3.16-1.1+deb7u13.</p>

<p>Nous vous recommandons de mettre à jour vos paquets graphicsmagick.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1159.data"
# $Id: $
