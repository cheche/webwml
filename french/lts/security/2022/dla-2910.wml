#use wml::debian::translation-check translation="067ea5d103d2179714a7ea64ab0a7caba605b0fe" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il y avait quatre problèmes dans ldns, une bibliothèque utilisée dans
les programmes liés au Domain Name System (DNS) :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-19860">CVE-2020-19860</a>

<p>Quand ldns version 1.7.1 vérifie un fichier de zone, la fonction
ldns_rr_new_frm_str_internal est sujette à une vulnérabilité de lecture de
tas hors limites. Un attaquant peut divulguer des informations sur le tas
en construisant une charge utile de fichier de zone.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-19861">CVE-2020-19861</a>

<p>Quand un fichier de zone dans ldns version 1.7.1 est analysé, la
fonction ldns_nsec3_salt_data est aussi considérée fiable pour la valeur de
longueur obtenue à partir du fichier de zone. Quand memcpy est copié, les
octets de données 0xfe - ldns_rdf_size(salt_rdf) peuvent être copiés,
provoquant une fuite d'informations de dépassement de tas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000231">CVE-2017-1000231</a>

<p>Une vulnérabilité de double libération de zone de mémoire dans parse.c
dans ldns 1.7.0 avait un impact non spécifié et des vecteurs d'attaque.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000232">CVE-2017-1000232</a>

<p>Une vulnérabilité de double libération de zone de mémoire dans
str2host.c dans ldns 1.7.0 avait un impact non spécifié et des vecteurs
d'attaque.</p></li>
</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces quatre problèmes ont été corrigés dans
la version 1.7.0-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ldns.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2910.data"
# $Id: $
