#use wml::debian::template title="Powody aby wybrać Debiana" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="7141cf5f25b03b1aa3c02c1afe2a5ca8cab2f672"

<p>Istnieje wiele powodów, dla których użytkownicy wybierają Debiana jako swój system operacyjny.</p>

<h1>Najważniejsze powody</h1>

# try to list reasons for end users first

<dl>
  <dt><strong>Debian jest Wolnym Oprogramowaniem.</strong></dt>
  <dd>
    Debian jest tworzony na bazie oprogramowania wolnego i o otwartym źródle
    i zawsze będzie w 100% oprogramowaniem <a href="free">wolnym</a>. Wolnym 
    dla każdego do używania, modyfikowania oraz rozprowadzania. To nasza główna
    obietnica złożona <a href="../users">naszym użytkownikom</a>. Jest także wolny od opłat.
  </dd>
</dl>

<dl>
  <dt><strong>Debian jest stabilnym i bezpiecznym systemem operacyjnym opartym na Linuksie.</strong></dt>
  <dd>
    Debian jest systemem operacyjnym dla szerokiej gamy urządzeń włączając w to 
    laptopy, komputery stacjonarne oraz serwery. Użytkownicy lubią jego stabilność i 
    niezawodność od 1993 roku. Zapewniamy rozsądną domyślną konfigurację każego pakietu. 
    Deweloperzy Debiana, w miarę możliwości, dostarczają aktualizacje bezpieczeństwa 
    dla wszystkich pakietów przez cały okres ich życia w systemie.
  </dd>
</dl>

<dl>
  <dt><strong>Debian ma rozbudowane wsparcie sprzętowe.</strong></dt>
  <dd>
    Większość sprzętu jest już wspierane przez jądro Linuksa. Jeżeli 
    wolne oprogramowanie jest niewystarczające dla jakiegoś sprzętu, 
    dostępne są sterowniki własnościowe. 
  </dd>
</dl>

<dl>
  <dt><strong>Debian zapewnia płynne aktualizacje.</strong></dt>
  <dd>
    Debian jest dobrze znany z łatwych i płynnych aktualizacji zarówno 
    w ramach jednego wydania jak i aktualizacji do następnego wydania.
  </dd>
</dl>

<dl>
  <dt><strong>Debian jest zalążkiem i podstawą wielu innych dystrybucji.</strong></dt>
  <dd>
    Wiele najpopularniejszych dystrybucji Linuksa, takich jak Ubuntu, Knoppix, 
    PureOS, SteamOS czy Tails wybrało Debiana jako podstawę swojego oprogramowania. 
    Debian zapewnia wszystkie narządzia, dzięki którym każdy może rozszerzyć pakiety 
    oprogramowania z archiwum Debiana o własne pakiety według swoich potrzeb.
  </dd>
</dl>

<dl>
  <dt><strong>Projekt Debian to społeczność.</strong></dt>
  <dd>
    Debian jest nie tylko systemem operacyjnym Linuks. Oprogramowanie jest 
    współtworzone przez setki wolontariuszy z całego świata. Możesz być 
    częścią społeczności Debiana nawet jeżeli nie jesteś programistą lub 
    administratorem. Debian jest oparty na społeczności i konsensusie i ma 
    <a href="../devel/constitution">demokratyczną strukturę zarządzania.</a>.
    Ponieważ wszyscy deweloperzy Debiana mają równe prawa, nie może on być 
    kontrolowany przez pojedyńczą firmę. Mamy deweloperów w ponad 60 krajach 
    i wspieramy tłumaczenia naszego Instalatora Debiana w ponad 80 językach. 
  </dd>
</dl>

<dl>
  <dt><strong>Debian ma wiele opcji instalatora.</strong></dt>
  <dd>
    Użytkownikcy końcowy będą korzystać z naszych płyt 
    <a href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">Live CD</a>
    zawierających łatwy w użyciu instalator Calamares, wymagający bardzo 
    niewielkiego wkładu lub wcześniejszej wiedzy. Bardziej zaawansowani użytkownicy 
    mogą skorzystać z naszego unikalnego, w pełni funkcjonalnego instalatora, a eksperci 
    mogą dostroić instalację, a nawet użyć zautomatyzowanego narzędzia do instalacji sieciowej.
  </dd>
</dl>

<br>

<h1>Środowisko Firmowe</h1>

<p>
  Jeżeli potrzebujesz Debiana w środowisku zawodowym, możesz skorzystać 
  z tych dodatkowych korzyści:
</p>

<dl>
  <dt><strong>Debian jest niezawodny.</strong></dt>
  <dd>
    Debian udowadnia swoją niezawodność każdego dnia w tysiącach rzeczywistych 
    scenariuszay, począwszy od laptopa dla jednego użytkownika, skończywszy na 
    super zderzaczach, giełdach, przemyśle samochodowym. Jest także popularny 
    w środowiku akademickim, w nauce i w sektorze publicznym.
  </dd>
</dl>

<dl>
  <dt><strong>Debian ma wielu ekspertów.</strong></dt>
  <dd>
    Nasi opiekunowie pakietów zajmują się nie tylko pakietami Debiana i włączaniem 
    nowych wersji aplikacji. Często są ekspertami w tych aplikacjach i bezpośrednio 
    współpracują przy ich rozwoju. Czasami są także częścią zespołu rozwijającego 
    aplikację.
  </dd>
</dl>

<dl>
  <dt><strong>Debian jest bezpieczny.</strong></dt>
  <dd>
    Debian posiada wsparcie bezpieczeństwa dla wydania stabilnego. Wiele innych 
    dystrybucji i badaczy bezpieczeństwa polega na narzędziu do śledzenia 
    bezpieczeństwa Debiana. 
  </dd>
</dl>

<dl>
  <dt><strong>Wsparcie Długoterminowe.</strong></dt>
  <dd>
    Istnieje bezpłatne <a href="https://wiki.debian.org/LTS">Wsparcie Długoterminowe</a>
    (LTS). Dostarcza ono rozszerzonego wsparcia dla wersji stablinej przez 5 i więcej lat. 
    Poza tym jest też inicjatywa <a href="https://wiki.debian.org/LTS/Extended">Rozszerzonego LTS</a>, 
    która rozszerza wsparcie dla ograniczonego zestawu pakietów powyżej 5 lat.
  </dd>
</dl>

<dl>
  <dt><strong>Obrazy dla środowisk chmurowych.</strong></dt>
  <dd>
    Oficjalne obrazy dla środowisk chmurowych są dostępne dla wszystkich 
    głównych platform. Zapewniamy też narzędzia i konfigurację aby można było 
    zbudować dostosowany do potrzeb własny obraz dla środowiska chmurowego. Można 
    także używać Debiana w wirtualnych maszynach lub w kontenerach.
  </dd>
</dl>

<br>

<h1>Deweloperzy</h1>
<p>Debian jest powszechnie używany przez wszelkiego rodzaju deweloperów oprogramowania i sprzętu.</p>

<dl>
  <dt><strong>Publicznie dostępny system śledzenia błędów.</strong></dt>
  <dd>
    Nasz Debianowy <a href="../Bugs">System śledzenia błędów</a> (BTS) jest 
    publicznie dostępny dla wszystkich przez przeglądarkę internetową. Nie 
    ukrywamy błędów naszego oprogramowania i z łatwością można przesyłać nowe 
    raporty o błędach.
  </dd>
</dl>

<dl>
  <dt><strong>Internet rzeczy (IoT) i urządzenia wbudowane</strong></dt>
  <dd>
    Wspieramy szeroką gamę urządzeń takich jak Raspberry Pi, różne wersje QNAP, 
    urządzenia mobilne, domowe rutery oraz wiele komputerów jednopłytowych 
    (Single Board Computers - SBC).
  </dd>
</dl>

<dl>
  <dt><strong>Wiele Architektur Sprzętowych.</strong></dt>
  <dd>
    Wsparcie dla <a href="../ports">długiej listy</a> architektur procesorów, 
    w tym amd64, i386, wiele wersji ARM i MIPS, POWER7, POWER8, IBM System Z,
    RISC-V. Debian jest także dostępny dla starszych i specyficznych, niszowych 
    architektur.
  </dd>
</dl>

<dl>
  <dt><strong>Ogromna liczba dostępnych pakietów oprogramowania.</strong></dt>
  <dd>
    Debiana ma dostępną największą liczbę pakietów do zainstalowania 
    (aktualnie <packages_in_stable>). Nasze pakiety wykorzystują format deb, 
    który słynie z wysokiej jakości. 
  </dd>
</dl>

<dl>
  <dt><strong>Różne opcje wydań.</strong></dt>
  <dd>
    Oprócz wydania stabilnego można użyć wydania testowego lub niestabilnego, w których 
    mamy najnowsze wersje oprogramowania. 
  </dd>
</dl>

<dl>
  <dt><strong>Wysoka jakość dzięki narzędziom programistycznym i polityce.</strong></dt>
  <dd>
    Kilka narzędzi programistycznych pomaga utrzymać jakość na wysokim poziomie, 
    a nasza <a href="../doc/debian-policy/">polityka</a> określa wymagania 
    techniczne jakie musi spełnić każdy pakiet, aby być włączonym do dystrybucji.
    Nasz system ciągłej integracji ma uruchomiony program autopkgtest, piuparts jest 
    narzędziem do sprawdzania instalacji, aktualizacji i usuwania pakietów, a lintian 
    jest narzędziem do wszechstronnego sprawdzania pakietów Debiana.
  </dd>
</dl>

<br>

<h1>Co mówią nasi użytkownicy</h1>

<ul style="line-height: 3em">
  <li>
    <q><strong>
      Dla mnie to idealny poziom łatwości użytkowania i stabilności. Na przestrzeni 
      lat używałem różnych dystrybucji, ale Debian jest jedynym, który po prostu działa. 
    </strong></q>
  </li>

  <li>
    <q><strong>
      Twardy jak skała. Mnóstwo paczek. Doskonała społeczność.
    </strong></q>
  </li>

  <li>
    <q><strong>
      Debian jest dla mnie symbolem stabilności i łatwości użytkowania.
    </strong></q>
  </li>
</ul>

