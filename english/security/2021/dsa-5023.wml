<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that modsecurity-apache, an Apache module to tighten
the Web application security, does not properly handles excessively
nested JSON objects, which could result in denial of service. The update
introduces a new <q>SecRequestBodyJsonDepthLimit</q> option to limit the
maximum request body JSON parsing depth which ModSecurity will accept
(defaults to 10000).</p>

<p>For the oldstable distribution (buster), this problem has been fixed
in version 2.9.3-1+deb10u1.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 2.9.3-3+deb11u1.</p>

<p>We recommend that you upgrade your modsecurity-apache packages.</p>

<p>For the detailed security status of modsecurity-apache please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/modsecurity-apache">https://security-tracker.debian.org/tracker/modsecurity-apache</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5023.data"
# $Id: $
