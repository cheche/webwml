<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilites have been discovered in the chromium web browser.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21159">CVE-2021-21159</a>

    <p>Khalil Zhani discovered a buffer overflow issue in the tab implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21160">CVE-2021-21160</a>

    <p>Marcin Noga discovered a buffer overflow issue in WebAudio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21161">CVE-2021-21161</a>

    <p>Khalil Zhani discovered a buffer overflow issue in the tab implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21162">CVE-2021-21162</a>

    <p>A use-after-free issue was discovered in the WebRTC implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21163">CVE-2021-21163</a>

    <p>Alison Huffman discovered a data validation issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21165">CVE-2021-21165</a>

    <p>Alison Huffman discovered an error in the audio implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21166">CVE-2021-21166</a>

    <p>Alison Huffman discovered an error in the audio implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21167">CVE-2021-21167</a>

    <p>Leecraso and Guang Gong discovered a use-after-free issue in the bookmarks
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21168">CVE-2021-21168</a>

    <p>Luan Herrera discovered a policy enforcement error in the appcache.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21169">CVE-2021-21169</a>

    <p>Bohan Liu and Moon Liang discovered an out-of-bounds access issue in the
    v8 javascript library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21170">CVE-2021-21170</a>

    <p>David Erceg discovered a user interface error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21171">CVE-2021-21171</a>

    <p>Irvan Kurniawan discovered a user interface error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21172">CVE-2021-21172</a>

    <p>Maciej Pulikowski discovered a policy enforcement error in the File
    System API.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21173">CVE-2021-21173</a>

    <p>Tom Van Goethem discovered a network based information leak.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21174">CVE-2021-21174</a>

    <p>Ashish Guatam Kambled discovered an implementation error in the Referrer
    policy.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21175">CVE-2021-21175</a>

    <p>Jun Kokatsu discovered an implementation error in the Site Isolation
    feature.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21176">CVE-2021-21176</a>

    <p>Luan Herrera discovered an implementation error in the full screen mode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21177">CVE-2021-21177</a>

    <p>Abdulrahman Alqabandi discovered a policy enforcement error in the
    Autofill feature.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21178">CVE-2021-21178</a>

    <p>Japong discovered an error in the Compositor implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21179">CVE-2021-21179</a>

    <p>A use-after-free issue was discovered in the networking implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21180">CVE-2021-21180</a>

    <p>Abdulrahman Alqabandi discovered a use-after-free issue in the tab search
    feature.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21181">CVE-2021-21181</a>

    <p>Xu Lin, Panagiotis Ilias, and Jason Polakis discovered a side-channel
    information leak in the Autofill feature.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21182">CVE-2021-21182</a>

    <p>Luan Herrera discovered a policy enforcement error in the site navigation
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21183">CVE-2021-21183</a>

    <p>Takashi Yoneuchi discovered an implementation error in the Performance API.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21184">CVE-2021-21184</a>

    <p>James Hartig discovered an implementation error in the Performance API.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21185">CVE-2021-21185</a>

    <p>David Erceg discovered a policy enforcement error in Extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21186">CVE-2021-21186</a>

    <p>dhirajkumarnifty discovered a policy enforcement error in the QR scan
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21187">CVE-2021-21187</a>

    <p>Kirtikumar Anandrao Ramchandani discovered a data validation error in
    URL formatting.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21188">CVE-2021-21188</a>

    <p>Woojin Oh discovered a use-after-free issue in Blink/Webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21189">CVE-2021-21189</a>

    <p>Khalil Zhani discovered a policy enforcement error in the Payments
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21190">CVE-2021-21190</a>

    <p>Zhou Aiting discovered use of uninitialized memory in the pdfium library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21191">CVE-2021-21191</a>

    <p>raven discovered a use-after-free issue in the WebRTC implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21192">CVE-2021-21192</a>

    <p>Abdulrahman Alqabandi discovered a buffer overflow issue in the tab
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21193">CVE-2021-21193</a>

    <p>A use-after-free issue was discovered in Blink/Webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21194">CVE-2021-21194</a>

    <p>Leecraso and Guang Gong discovered a use-after-free issue in the screen
    capture feature.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21195">CVE-2021-21195</a>

    <p>Liu and Liang discovered a use-after-free issue in the v8 javascript
    library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21196">CVE-2021-21196</a>

    <p>Khalil Zhani discovered a buffer overflow issue in the tab implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21197">CVE-2021-21197</a>

     <p>Abdulrahman Alqabandi discovered a buffer overflow issue in the tab
     implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21198">CVE-2021-21198</a>

    <p>Mark Brand discovered an out-of-bounds read issue in the Inter-Process
    Communication implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21199">CVE-2021-21199</a>

    <p>Weipeng Jiang discovered a use-after-free issue in the Aura window and
    event manager.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 89.0.4389.114-1~deb10u1.</p>

<p>We recommend that you upgrade your chromium packages.</p>

<p>For the detailed security status of chromium please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4886.data"
# $Id: $
