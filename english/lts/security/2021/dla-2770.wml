<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were fixed in the chat client WeeChat.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8955">CVE-2020-8955</a>

    <p>A crafted irc message 324 (channel mode) could result in a crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9759">CVE-2020-9759</a>

    <p>A crafted irc message 352 (who) could result in a crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9760">CVE-2020-9760</a>

    <p>A crafted irc message 005 (setting a new mode for a nick) could result in a crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40516">CVE-2021-40516</a>

    <p>A crafted WebSocket frame could result in a crash in the Relay plugin.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1.6-1+deb9u3.</p>

<p>We recommend that you upgrade your weechat packages.</p>

<p>For the detailed security status of weechat please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/weechat">https://security-tracker.debian.org/tracker/weechat</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2770.data"
# $Id: $
