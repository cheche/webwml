<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability was discovered in PHP, a server-side, HTML-embedded
scripting language.  When PHP is processing incoming HTTP cookie values,
the cookie names are url-decoded. This may lead to cookies with prefixes
like __Host confused with cookies that decode to such prefix, thus
leading to an attacker being able to forge a cookie which is supposed to
be secure.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
7.0.33-0+deb9u10.</p>

<p>We recommend that you upgrade your php7.0 packages.</p>

<p>For the detailed security status of php7.0 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/php7.0">https://security-tracker.debian.org/tracker/php7.0</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2397.data"
# $Id: $
