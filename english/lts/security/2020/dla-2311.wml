<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential cross-site scripting
vulnerability via <tt>iframe</tt> HTML elements in
<a href="https://www.zabbix.com/">Zabbix</a>, a PHP-based monitoring
system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15803">CVE-2020-15803</a>

    <p>Zabbix before 3.0.32rc1, 4.x before 4.0.22rc1, 4.1.x through 4.4.x before 4.4.10rc1, and 5.x before 5.0.2rc1 allows stored XSS in the URL Widget.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1:3.0.7+dfsg-3+deb9u1.</p>

<p>We recommend that you upgrade your zabbix packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2311.data"
# $Id: $
