<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several issues in gdk-pixbuf, a library to handle pixbuf, have been found.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6352">CVE-2016-6352</a>

      <p>fix for denial of service (out-of-bounds write and crash) via
      crafted dimensions in an ICO file</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2870">CVE-2017-2870</a>

      <p>Fix for an exploitable integer overflow vulnerability in the
      tiff_image_parse functionality. When software is compiled with
      clang, A specially crafted tiff file can cause a heap-overflow
      resulting in remote code execution. Debian package is compiled
      with gcc and is not affected, but probably some downstream is.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6312">CVE-2017-6312</a>

      <p>Fix for an integer overflow in io-ico.c that allows attackers
      to cause a denial of service (segmentation fault and application
      crash) via a crafted image</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6313">CVE-2017-6313</a>

      <p>Fix for an integer underflow in the load_resources function in
      io-icns.c that allows attackers to cause a denial of service
      (out-of-bounds read and program crash) via a crafted image entry
      size in an ICO file</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6314">CVE-2017-6314</a>

      <p>Fix for an infinite loop in the make_available_at_least function
      in io-tiff.c that allows attackers to cause a denial of service
      via a large TIFF file.</p>


<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.31.1-2+deb8u8.</p>

<p>We recommend that you upgrade your gdk-pixbuf packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2043.data"
# $Id: $
