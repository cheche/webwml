#use wml::debian::template title="Atualizando para o Debian 2.0 em máquinas x86"
#use wml::debian::translation-check translation="8da95139c3595d47371ba8d288784086ae2ebacd"

<p>Para evitar problemas na atualização de pacotes via dpkg, dselect ou dftp
(devido a possíveis conflitos da libc5/libc6), é recomendado um procedimento
especial de atualização. Este documento descreve esse procedimento.

<p>Existem várias maneiras de atualizar a partir de uma versão anterior:
  <ol>
  <li>autoup.sh<br>
       Este é um script que irá atualizar os programas na ordem correta e até
     baixar os arquivos deb para você. Devido às mudanças contínuas no
     repositório, é fornecido um arquivo tar dos pacotes que estavam disponíveis
     no momento em que o autoup.sh foi lançado pela última vez. Ele está
     disponível nos seguintes sites:
        <ul>
        <li><a href="https://www.debian.org/releases/hamm/autoup/">https://www.debian.org/releases/hamm/autoup/</a></li>
        <li><a href="http://archive.debian.org/debian/dists/hamm/main/upgrade-i386/">http://archive.debian.org/debian/dists/hamm/main/upgrade-i386/</a></li>
        </ul>
  <li>apt-get<br>
       Este é a parte da linha de comando do futuro gerenciador de pacotes do
     Debian. Ele sabe como solicitar pacotes e fará o download deles a partir
     de um repositório local, sites http e ftp. Ele é capaz de mesclar as
     informações de vários locais, para que você possa usar um CD, um espelho
     atualizado e um site fora dos EUA para obter a melhor combinação de
     velocidade, variedade e versões mais recentes.
     Apenas execute 'apt-get update; apt-get dist-upgrade'.
     A versão do 'Bo' está em:
     <a href="http://archive.debian.org/debian/dists/hamm/main/upgrade-i386/">http://archive.debian.org/debian/dists/hamm/main/upgrade-i386/</a>.
     A versão do apt-get no Bo está bem testado. O apt-get é
     oficialmente parte do sistema de empacotamento a partir do slink.
  <li>Fazendo na mão<br>
       Um HOWTO pode ser visto em:
     <a href="$HOME/releases/hamm/autoup/libc5-libc6-Mini-HOWTO.html">
     $HOME/releases/hamm/autoup/libc5-libc6-Mini-HOWTO.html</a>
     mas como o autoup.sh simplesmente automatiza esse processo, essa é a opção
     menos conveniente.
  </ol>

<H3>Perguntas e respostas</H3>
<pre>
P: Por que não usar o método ftp do dselect normalmente?
R: Ele não solicitará a instalação do pacote corretamente, portanto não poderá
   garantir uma atualização suave. Estamos trabalhando no APT, uma nova
   interface para o dpkg. Com o apt todas as futuras atualizações serão mais
   fáceis do que nunca e não sofrerão com esse tipo de inconveniência.

P: Ok, fiz a atualização, estou seguro(a) para fazer as coisas novamente?
R: Sim, você pode usar o dselect novamente.
</pre>
